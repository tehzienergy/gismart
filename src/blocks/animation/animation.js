window.sr = ScrollReveal();

sr.reveal('.animation--fadeIn', { 
  origin: 'left',
  duration: 2000,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  distance: '0',
  scale: 1,
  delay: 700,
  mobile: false,
  reset: false,
  opacity: 0,
  viewFactor: .1
});

sr.reveal('.animation--fadeInFast', { 
  origin: 'left',
  duration: 2000,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  distance: '0',
  scale: 1,
  delay: 0,
  mobile: false,
  reset: false,
  opacity: 0,
  viewFactor: .3
});

sr.reveal('.animation--fadeUp', { 
  origin: 'bottom',
  duration: 1000,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  distance: '100px',
  scale: 1,
  delay: 0,
  mobile: false,
  reset: false,
  opacity: 0,
  viewFactor: .5,
  interval: 100
});
