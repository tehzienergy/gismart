$('input[type=file]').change(function() {
  var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
  $(this).closest('.custom-file').addClass('custom-file--active').find('.custom-file-label').text(filename);
});

$('.custom-file-remove').click(function(e) {
  e.preventDefault();
  $(this).closest('.custom-file').removeClass('custom-file--active');
  $(this).closest('.custom-file').find('.custom-file-input').val();
  $(this).closest('.custom-file').find('.custom-file-label').text('Attach your CV');
});
