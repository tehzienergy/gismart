$(".front__goto").click(function(e) {
    e.preventDefault();
    var aim = $(this).attr("href");
    $('html, body').animate({scrollTop: $(aim).offset().top},'slow');
});
