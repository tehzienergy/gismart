if ($(window).width() < 1250) {
  $('.menu__item').click(function(e) {
    e.preventDefault();
    $(this).siblings().find('.menu__drop').fadeOut('fast');
    $(this).find('.menu__drop').fadeToggle('fast');
  });
}
