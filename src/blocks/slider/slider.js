$(document).ready(function(){
  $('#slider').slick({
    dots: true,
    infinite: true,
    appendArrows: '#slider-controls',
    appendDots: '#slider-dots',
    fade: true
  });
});
