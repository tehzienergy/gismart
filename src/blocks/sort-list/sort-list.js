$(document).ready(function(){
  var list = $('#sort');
  var item = list.find('.sort-list__item');
  var location = $('#sort-location');
  var role = $('#sort-role');
  var count = $('#sort-count');

  role.change(function(e){
    filterList(location.val(),role.val());
  });

  location.change(function(e){
    filterList(location.val(),role.val());
  });

  function filterList(location,role){
    var filtrLocation = '[data-location="'+location+'"]';
    var filtrRole = '[data-role="'+role+'"]';
    var result;
    location = (location=='')? 'all' : location;
    role = (role=='')? 'all' : role;
    if (location==='all' && role==='all'){
      item.fadeIn('fast');
      count.html(item.length || 0);
    }
    else if(location==='all'){
      item.fadeOut('fast');
      result = item.filter(filtrRole).fadeIn('fast');
      count.html(result.length || 0);
    }
    else if(role==='all'){
      item.fadeOut('fast');
      result = item.filter(filtrLocation).fadeIn('fast');
      count.html(result.length || 0);
    }
    else {
      item.fadeOut('fast');
      result = item.filter(filtrLocation).filter(filtrRole).fadeIn('fast');
      count.html(result.length || 0);
    }
  }
});