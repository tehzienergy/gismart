if ($(window).width() < 1250) {
  $('.menu-item-has-children > a').click(function(e) {
    e.preventDefault();
    $(this).closest('.menu-item').siblings().find('.sub-menu').fadeOut('fast');
    $(this).closest('.menu-item').find('.sub-menu').fadeToggle('fast');
  });
}


$(document).ready(function () {
  $('.js-select').each(function (i, el) {
    $(el).select2({
      placeholder: $(el).data('placeholder') || null,
      dropdownPosition: 'below',
      minimumResultsForSearch: Infinity,
      width: '100%'
    }).on('select2:open', function () {
      $('.select2-dropdown').removeClass('select2-dropdown--above');
      $('.select2-dropdown').addClass('select2-dropdown--below');
      
      setTimeout(function() {
        var ps = new PerfectScrollbar('.select2-results__options',{
          suppressScrollX: true,
          wheelspeed: 0.1
        });
      }, 10)
      
      setTimeout(function() {
        $('.select2-results__options').focus();
      }, 100)
    })
  });
});

//$(window).scroll(function () {
//  $('.js-select').select2('close');
//});

$('.js-select').on('select2:open', function() {
  if($(this).val() != 'all') {
    $('.select2-results__options').removeClass('select2-results__options--all');
  }
  else {
    $('.select2-results__options').addClass('select2-results__options--all');
  }
});


window.sr = ScrollReveal();

sr.reveal('.animation--fadeIn', { 
  origin: 'left',
  duration: 2000,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  distance: '0',
  scale: 1,
  delay: 700,
  mobile: false,
  reset: false,
  opacity: 0,
  viewFactor: .1
});

sr.reveal('.animation--fadeInFast', { 
  origin: 'left',
  duration: 2000,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  distance: '0',
  scale: 1,
  delay: 0,
  mobile: false,
  reset: false,
  opacity: 0,
  viewFactor: .3
});

sr.reveal('.animation--fadeUp', { 
  origin: 'bottom',
  duration: 1000,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  distance: '100px',
  scale: 1,
  delay: 0,
  mobile: false,
  reset: false,
  opacity: 0,
  viewFactor: .5,
  interval: 100
});
